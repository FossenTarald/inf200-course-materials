{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# INF200 Lecture No Ja03\n",
    "### Hans Ekkehard Plesser / NMBU\n",
    "### 11 January 2023\n",
    "\n",
    "## Today's topics\n",
    "\n",
    "- Keeping your code tidy\n",
    "- More on Python \n",
    "    - Deleting from lists\n",
    "    - `isinstance()` considered harmful\n",
    "    - Creating new subclass instances elegantly\n",
    "    - A little more on randomness\n",
    "- More on testing\n",
    "    - Levels of testing\n",
    "    - Test file names\n",
    "    - Placement of test files\n",
    "    - Suggestions for test design\n",
    "    - Approximate comparisons\n",
    "    - Test parameterization\n",
    "    - Test classes with setup and teardown features\n",
    "    - Mocking\n",
    "    - Tests involving randomness"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "-------\n",
    "\n",
    "# Keeping your code tidy\n",
    "\n",
    "- Run `Code > Inspect code` regularly on your code\n",
    "- Fix weaknesses reported\n",
    "- Also keep an eye on typos\n",
    "- Formatting checks are run automatically on GitLab (details soon)\n",
    "- To locally run exactly the same tests that are run on GitLab\n",
    "    1. open a `Terminal` in PyCharm\n",
    "    1. run `flake8 src tests`\n",
    "- In-class example: `examples/biolab_project`\n",
    "\n",
    "## Getting the right `flake8` version\n",
    "\n",
    "- The tests applied by `flake8` can change with `flake8` versions.\n",
    "- To get consistent results, it can be useful to fix the `flake8` version.\n",
    "- We will use the currently newest version, 6.0.\n",
    "- Problem: conda only provides `flake8` version 4.0.1\n",
    "- Solution: install `flake8` using `pip`\n",
    "    - The original `inf200_conda_env.yml` does this\n",
    "    - New problem: that file also installs `sphinx` via `pip`, fails on some Windows systems\n",
    "\n",
    "### Checking the flake8 version\n",
    "\n",
    "In a terminal with the conda environment you use for BioSim activated, run\n",
    "```\n",
    "flake8 --version\n",
    "```\n",
    "This will output a line like\n",
    "```\n",
    "6.0.0 (mccabe: 0.7.0, pycodestyle: 2.10.0, pyflakes: 3.0.1) CPython 3.10.8 on Darwin\n",
    "```\n",
    "If the line starts with `6.0.x`, you are all set.\n",
    "\n",
    "### Fix: update flake8 with pip\n",
    "\n",
    "If you have a different `flake8` version than some 6.0 version, run with the conda environment active\n",
    "```\n",
    "pip install \"flake8==6.0\"\n",
    "```\n",
    "Note the `\"\"`, they are important, and it must be `==`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "---------------\n",
    "# More on Python\n",
    "\n",
    "## Deleting from lists\n",
    "\n",
    "- Removing elements from a list inside a loop over the list is dangerous\n",
    "- It can confuse the list iteration\n",
    "- Example: remove numbers that can be divide by 2 or 3\n",
    "\n",
    "### A correct loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Testing 0 ... divisible\n",
      "Testing 1\n",
      "Testing 2 ... divisible\n",
      "Testing 3 ... divisible\n",
      "Testing 4 ... divisible\n",
      "Testing 5\n",
      "Testing 6 ... divisible\n",
      "Testing 7\n",
      "Testing 8 ... divisible\n",
      "Testing 9 ... divisible\n"
     ]
    }
   ],
   "source": [
    "d = list(range(10))\n",
    "for n in d:\n",
    "    print('Testing', n, end='')\n",
    "    if n % 2 == 0 or n % 3 == 0:\n",
    "        print(' ... divisible', end='')\n",
    "    print()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A confused loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Testing 0\n",
      "Testing 2\n",
      "Testing 4\n",
      "Testing 6\n",
      "Testing 8\n",
      "[1, 3, 5, 7, 9]\n"
     ]
    }
   ],
   "source": [
    "d = list(range(10))\n",
    "for n in d:\n",
    "    print('Testing', n)\n",
    "    if n % 2 == 0 or n % 3 == 0:\n",
    "        d.remove(n)\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A better solution: keep the good ones"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 5, 7]\n"
     ]
    }
   ],
   "source": [
    "d = list(range(10))\n",
    "d = [n for n in d if not (n % 2 == 0 or n % 3 == 0)]\n",
    "print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## `isinstance()` considered harmful\n",
    "\n",
    "- Do not use \n",
    "    - `if isinstance() ...` \n",
    "    - `if type() == ...`\n",
    "    - `if cell.code == 'L' ...`\n",
    "\n",
    "- If you are tempted to do so, in 99.9% of cases you are trying to hack a solution that could be achieved much more elegantly and robustly using proper object oriented design.\n",
    "- An object shall know itself how to \"behave\" (through proper member functions).\n",
    "- For historical reference on the headline, see https://en.wikipedia.org/wiki/Considered_harmful."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Creating new subclass instances elegantly (aka \"birth\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {},
   "outputs": [],
   "source": [
    "import random"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A class allowing instances to create new instances of the class\n",
    "\n",
    "New objects are created\n",
    "- with a given probability $p$\n",
    "- may fail to be created if the chosen \"weight\" is too large\n",
    "- `clone()` returns new object or `None`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {},
   "outputs": [],
   "source": [
    "class Q:\n",
    "    \n",
    "    p = 0.5\n",
    "    max_w = 5\n",
    "    \n",
    "    def __init__(self, w):\n",
    "        assert w > 0\n",
    "        self.w = w \n",
    "        \n",
    "    def __repr__(self):\n",
    "        return f'Q({self.w:.2g})'\n",
    "        \n",
    "    def clone(self):\n",
    "        if random.random() < self.p:\n",
    "            nw = random.lognormvariate(0, 1)\n",
    "            if nw <= self.max_w:\n",
    "                return Q(nw)\n",
    "        return None"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[None, None, Q(1.4), Q(0.43), Q(3.1), None, Q(0.83), None, None, None]"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "random.seed(123456)\n",
    "q = Q(10)\n",
    "[q.clone() for _ in range(10)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A class hierarchy with similar properties\n",
    "\n",
    "- `A` is an abstract base class \n",
    "- Only objects of subclasses `B` and `C` can be instantiated\n",
    "- Cloning is still done in the base class using `type()`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "metadata": {},
   "outputs": [],
   "source": [
    "class A:\n",
    "    \n",
    "    p = None\n",
    "    max_w = None\n",
    "    \n",
    "    def __init__(self, w):\n",
    "        assert w > 0\n",
    "        self.w = w \n",
    "        \n",
    "    def __repr__(self):\n",
    "        return f'{type(self).__name__}({self.w:.2g})'\n",
    "        \n",
    "    def clone(self):\n",
    "        if random.random() < self.p:\n",
    "            nw = random.lognormvariate(0, 1)\n",
    "            if nw <= self.max_w:\n",
    "                return type(self)(nw)\n",
    "        return None        \n",
    "\n",
    "class B(A):\n",
    "    p = 0.5\n",
    "    max_w = 5\n",
    "\n",
    "class C(A):\n",
    "    p = 0.3\n",
    "    max_w = 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[B(0.58), B(1.1), B(0.77), B(1.2), None, B(4), None, B(1.6), B(0.29), None]"
      ]
     },
     "execution_count": 10,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "random.seed(12345)\n",
    "b = B(10)\n",
    "[b.clone() for _ in range(10)]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[None, C(2.2), None, C(1.1), C(0.77), C(1.2), None, None, None, C(1.6)]"
      ]
     },
     "execution_count": 11,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "random.seed(12345)\n",
    "c = C(10)\n",
    "[c.clone() for _ in range(10)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### A function to produce many clones\n",
    "\n",
    "- Takes a list of objects\n",
    "- Gives every object the opportunity to clone\n",
    "- Returns list of only those objects that were cloned (drops `None`s)\n",
    "\n",
    "#### First implementation: explicit loop"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mc1(d):\n",
    "    r = []\n",
    "    for x in d:\n",
    "        xc = x.clone()\n",
    "        if xc:\n",
    "            r.append(xc)\n",
    "    return r"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Second implementation: list comprehension\n",
    "\n",
    "- Use *assignment expression*\n",
    "- Works only in Python 3.8 and later"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "metadata": {},
   "outputs": [],
   "source": [
    "def mc2(d):\n",
    "    return [xc for x in d if (xc := x.clone())]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Test both implementations"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[B(10), C(20), B(10), B(0.58), C(1.1), B(0.77)]\n",
      "[B(10), C(20), B(10), B(0.58), C(1.1), B(0.77)]\n"
     ]
    }
   ],
   "source": [
    "for f in [mc1, mc2]:\n",
    "    random.seed(12345)\n",
    "    d = [B(10), C(20), B(10)]\n",
    "    new_d = f(d)\n",
    "    d.extend(new_d)\n",
    "    print(d)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "## A little more on randomness\n",
    "\n",
    "### Obtaining uniform random numbers on $[0, 1)$\n",
    "\n",
    "- Use `random.random()` for this, even though [documentation](https://docs.python.org/3/library/random.html#real-valued-distributions) is not entirely clear (but see [here](https://docs.python.org/3/library/random.html#))\n",
    "\n",
    "### Drawing lognormally distributed weights\n",
    "- Use `random.lognormvariate(mu, sigma)` to draw birth weights with\n",
    "$$\n",
    "\\mu = \\ln\\left(\\frac{\\mu_X^2}{\\sqrt{\\mu_X^2+\\sigma_X^2}}\\right)\n",
    "\\qquad\\text{and}\\qquad\n",
    "\\sigma = \\sqrt{\\ln\\left(1+\\frac{\\sigma_X^2}{\\mu_X^2}\\right)}\n",
    "$$\n",
    "where $\\mu_X$=`w_birth`and $\\sigma_X$=`sigma_birth` ([equations after Wikipedia](https://en.wikipedia.org/wiki/Log-normal_distribution#Generation_and_parameters)).\n",
    "\n",
    "### Choosing between two alternatives\n",
    "\n",
    "- An animal has a probability $p$ to die\n",
    "- How do we decided if the animal will die in a given year?\n",
    "    - Draw uniformly distributed random number from $[0, 1)$ and compare to $p$\n",
    "    \n",
    "### Choosing between multiple alternaives\n",
    "\n",
    "- Literature: Knuth, The Art of Computer Programming, vol 2, ch 3.3-3.4 \n",
    "- In a simluation, we want to choose between four alternatives with probabilities $p_0, p_1, p_2, p_3$\n",
    "- Note $\\sum_{n=0}^3 p_n = 1$ by definition\n",
    "- Cumulative probabilities $P_n = \\sum_{k=0}^n p_k$ divide unit interval in sections corresponding to events 0, 1, 2, 3\n",
    "- Specifically, we choose a random number $r$ and select \n",
    "\n",
    "\\begin{equation}\n",
    "\\begin{cases}\n",
    "\\text{event}\\: 0 \\quad\\text{if}\\; r < P_0 \\\\\n",
    "\\text{event}\\: n \\quad\\text{if}\\; P_{n-1} \\leq r < P_{n}\\;\\; \\text{for}\\; n>0\n",
    "\\end{cases}\n",
    "\\end{equation}\n",
    "\n",
    "- The following code will select from `len(p)` alternatives with probabilities `p[0]`, `p[1]`, ..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {},
   "outputs": [],
   "source": [
    "def random_select(p):\n",
    "    r = random.random()\n",
    "    n = 0\n",
    "    while r >= p[n]:\n",
    "        r -= p[n]\n",
    "        n += 1\n",
    "    return n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Simpler approach for our simulations\n",
    "\n",
    "- Animals move in all four directions with *same* probability\n",
    "- Can use `random.choice()` to pick one element from a list with equal probability"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "tags": []
   },
   "source": [
    "------------------\n",
    "\n",
    "# More on testing\n",
    "\n",
    "## Levels of testing\n",
    "\n",
    "- *unit tests* are tests of small parts of code\n",
    "    - test individual methods\n",
    "- *integration tests* test that the parts of a larger project work together\n",
    "    - test that class instances behave as expected\n",
    "    - expect that a class, e.g., representing a landscape cell, properly manages animals\n",
    "- *acceptance tests* test that the software as a whole\n",
    "    - `check_sim.py`\n",
    "    - `test_biosim_interface.py`\n",
    "    - similar simulations, e.g., with parameter modifications\n",
    "        - different islands and initial populations\n",
    "        - parameter choices preventing birth, death, eating, movement, ...\n",
    "- *regression tests* are added when a bug is discovered\n",
    "    - the test reproduces the bug\n",
    "    - when the bug is fixed, the test passes\n",
    "    - we keep the test, in case we should re-introduce the bug by a later change (regression)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "\n",
    "## Test file names\n",
    "\n",
    "- You should write different test modules (files) to keep everything neat and organized\n",
    "- Rule of thumb: One test module for each module in your package\n",
    "    - `animals.py` ---> `test_animals.py`\n",
    "    - `landscape.py` ---> `test_landscape.py`\n",
    "    - ...\n",
    "- Each individual test should have a descriptive name\n",
    "    - When a test fails, the first thing you read is the name\n",
    "        - Should describe what was tested and failed\n",
    "    - Should write a docstring to further explain the test\n",
    "    \n",
    "## Placement of test files\n",
    "\n",
    "- Two alternatives, no definite \"best\" solution\n",
    "- See course repository `examples`\n",
    "- Both variants can be run in the same way from PyCharm by adding a suitable PyTest configuration\n",
    "- **We will use variant 1**\n",
    "\n",
    "### Variant 1: tests parallel to code directory\n",
    "\n",
    "- Based on recommendations by the [Python Packaging Project](https://packaging.python.org/en/latest/tutorials/packaging-projects/)\n",
    "\n",
    "```\n",
    "chutes_project/\n",
    "   src/\n",
    "       chutes/\n",
    "          __init__.py\n",
    "          board.py\n",
    "          ...\n",
    "   examples/\n",
    "   tests/\n",
    "      test_board.py\n",
    "   setup.py\n",
    "```\n",
    "- `tests` is a directory \"parallel\" to `chutes` code directory\n",
    "- `tests` is *not* a package\n",
    "- Test files use absolute imports\n",
    "```python\n",
    "from chutes.board import Board\n",
    "```\n",
    "- PyTest configuration in PyCharm should cover `tests` directory\n",
    "\n",
    "\n",
    "### Variant 2: tests in code directory\n",
    "```\n",
    "chutes_project_alt/\n",
    "    src/\n",
    "        chutes/\n",
    "          __init__.py\n",
    "          board.py\n",
    "          ...\n",
    "          tests/\n",
    "             __init__.py\n",
    "             test_board.py\n",
    "   examples/\n",
    "   setup.py\n",
    "```\n",
    "- `tests` is subdirectory of `chutes` code directory\n",
    "- `tests` is a package (contains `__init__.py`)\n",
    "- Test files use relative imports\n",
    "```python\n",
    "from ..board import Board\n",
    "```\n",
    "- PyTest configuration in PyCharm should cover `chutes/tests` directory"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "\n",
    "## Suggestions for test design\n",
    "\n",
    "- Test code should be simple: if you cannot understand a test, it is not worth much\n",
    "- Have only a single `assert` in each test: the test fails on the first failing assert, all checks in later asserts will not be performed\n",
    "- If you use \"magic values\", document how you obtained them or best, compute them explicitly (but do not copy-paste code!)\n",
    "- Use variables for input values instead of literal numbers—improved reliability\n",
    "\n",
    "### Poor example\n",
    "\n",
    "```python\n",
    "def test_growing():\n",
    "    a = Baby()\n",
    "    for _ in range(10):\n",
    "        a.grow()\n",
    "    assert a.age == 10\n",
    "    assert a.height == 55\n",
    "```\n",
    "\n",
    "### Good example\n",
    "\n",
    "```python\n",
    "def test_age_increase():\n",
    "    num_days = 10\n",
    "    baby = Baby()\n",
    "    for _ in range(num_days):\n",
    "        baby.grow()\n",
    "    assert baby.age == num_days\n",
    "    \n",
    "def test_height_increase():\n",
    "    num_days = 10\n",
    "    baby = Baby()\n",
    "    for _ in range(num_days):\n",
    "        baby.grow()\n",
    "    assert baby.height == baby.birth_height + num_days * baby.growth_rate\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Approximate comparisons"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {},
   "outputs": [],
   "source": [
    "from pytest import approx"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Check if two numbers are equal to within a relative error of $10^{-6}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 18,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "3.001 == approx(3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 19,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 19,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "3.0000001 == approx(3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Comparing to zero uses absolute error of $10^{-12}$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "False"
      ]
     },
     "execution_count": 20,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "0.0001 == approx(0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 21,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 21,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "0.0000000000001 == approx(0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Approximate comparisons also work for composite data types:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 22,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 22,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "[1.000001, 3] == approx([1.000001, 3]) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 23,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "{'a': 1.000001, 'b': 3} == approx({'a': 1.000001, 'b': 3}) "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 24,
   "metadata": {},
   "outputs": [
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 24,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "np.array([1.000001, 3]) == approx(np.array([1.000001, 3]))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "See https://docs.pytest.org/en/latest/reference.html#pytest-approx for details."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Test parameterization\n",
    "\n",
    "- Parameterize tests: run one test several times with different values\n",
    "- For more information, see http://pytest.readthedocs.io/en/latest/parametrize.html#parametrize\n",
    "    \n",
    "### Poor example\n",
    "\n",
    "```python\n",
    "def test_default_board_adjustments():\n",
    "    \"\"\"Some tests on default board.\"\"\"\n",
    "\n",
    "    brd = Board()\n",
    "    assert brd.position_adjustment(1) == 39\n",
    "    assert brd.position_adjustment(2) == 0\n",
    "    assert brd.position_adjustment(33) == -30\n",
    "```\n",
    "\n",
    "### Better solution with parameterization\n",
    "\n",
    "```python\n",
    "@pytest.mark.parametrize(\"from_pos, to_pos\",\n",
    "                         [[1, 40],\n",
    "                          [2, 2],\n",
    "                          [33, 3]])\n",
    "def test_default_board_adjustments(from_pos, to_pos):\n",
    "    \"\"\"Test chutes and ladders on default board.\"\"\"\n",
    "\n",
    "    brd = Board()\n",
    "    assert from_pos + brd.position_adjustment(from_pos) == to_pos\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Test classes with setup and teardown fixtures\n",
    "\n",
    "- We can combine tests that are related into a class\n",
    "- The class name must begin with `Test`\n",
    "- Each method with a name beginning with `test_` will be run as a test\n",
    "- Methods with other names can be used as helpers\n",
    "- Most important helpers: setup and teardown fixtures\n",
    "    - http://pytest.readthedocs.io/en/latest/fixture.html#fixture\n",
    "- How it works\n",
    "    - Create method that does preparation for tests or cleanup after tests\n",
    "    - Mark method as PyTest fixture with `@pytest.fixture` decorator\n",
    "    - Fixtures with `autouse=True` will be applied to every test in the class\n",
    "    - Other fixtures will only be used if passed to a test method\n",
    "    - Code before `yield` is run before the test (setup)\n",
    "    - Code after `yield` is run after the test (teardown), independent of whether the test fails or not\n",
    "    - If there is no `yield`, the method only performs setup\n",
    "- See `january_block/examples/biolab_project` for examples\n",
    "- Note: fixtures can also be defined at the module level, but then it is difficult to share objects created during setup with the tests\n",
    "\n",
    "```python\n",
    "class TestDeathDivision:\n",
    "    \n",
    "    @pytest.fixture(autouse=True)\n",
    "    def create_dish(self):\n",
    "        self.n_a = 10\n",
    "        self.n_b = 20\n",
    "        self.dish = Dish(self.n_a, self.n_b)\n",
    "\n",
    "    @pytest.fixture\n",
    "    def reset_bacteria_defaults(self):\n",
    "        # no setup\n",
    "        yield\n",
    "\n",
    "        # reset class parameters to default values after each test\n",
    "        Bacteria.set_params(Bacteria.default_params)\n",
    "\n",
    "    def test_death(self):\n",
    "        n_a_old = self.dish.get_num_a()\n",
    "        n_b_old = self.dish.get_num_b()\n",
    "\n",
    "        for _ in range(10):\n",
    "            self.dish.death()\n",
    "            n_a = self.dish.get_num_a()\n",
    "            n_b = self.dish.get_num_b()\n",
    "            # n_a and n_b must never increase\n",
    "            assert n_a <= n_a_old and n_b <= n_b_old\n",
    "            n_a_old, n_b_old = n_a, n_b\n",
    "\n",
    "    def test_division(self):\n",
    "        n_a_old = self.dish.get_num_a()\n",
    "        n_b_old = self.dish.get_num_b()\n",
    "\n",
    "        for _ in range(10):\n",
    "            self.dish.division()\n",
    "            n_a = self.dish.get_num_a()\n",
    "            n_b = self.dish.get_num_b()\n",
    "            # n_a and n_b must never decrease\n",
    "            assert n_a >= n_a_old and n_b >= n_b_old\n",
    "            n_a_old, n_b_old = n_a, n_b\n",
    "\n",
    "    def test_all_die(self, reset_bacteria_defaults):\n",
    "        Bacteria.set_params({'p_death': 1.0})\n",
    "        self.dish.death()\n",
    "        assert self.dish.get_num_a() == 0 and self.dish.get_num_b() == 0\n",
    "\n",
    "    @pytest.mark.parametrize(\"n_a, n_b, p_death\",\n",
    "                             [[100, 200, 0.1],\n",
    "                              [100, 200, 0.9],\n",
    "                              [10, 20, 0.5]])\n",
    "    def test_death(self, reset_bacteria_defaults, n_a, n_b, p_death):\n",
    "\n",
    "        Bacteria.set_params({'p_death': p_death})\n",
    "        dish = Dish(n_a, n_b)\n",
    "        dish.death()\n",
    "        died_a = n_a - dish.get_num_a()\n",
    "        died_b = n_b - dish.get_num_b()\n",
    "\n",
    "        pass_a = binom_test(died_a, n_a, p_death) > ALPHA\n",
    "        pass_b = binom_test(died_b, n_b, p_death) > ALPHA\n",
    "        \n",
    "        assert pass_a and pass_b\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Mocking\n",
    "\n",
    "- Temporarily replace a Python object with a different one, typically replacing a class or method\n",
    "- Supported by Python `unittest.mock`\n",
    "    - Relatively complex\n",
    "    - We will not use it directly\n",
    "    - For documentation, see\n",
    "        - https://docs.python.org/3/library/unittest.mock-examples.html\n",
    "        - https://docs.python.org/3/library/unittest.mock.html#the-mock-class\n",
    "- For convenient mocking with py.test, we need a py.test extension `pytest-mock`\n",
    "    - For documentation, see https://pytest-mock.readthedocs.io/en/latest/\n",
    "- Mocking is most useful for \"glass box\" testing, since we need to know which functions to mock.\n",
    "    - Useful if functions are not available yet but we want to test code calling them\n",
    "- **Use sparingly**, focus on tests on *what* methods do, not *how* they do it"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: Replacing random generator with fixed value\n",
    "\n",
    "- See also `chutes_project/tests/test_player.py`\n",
    "- In the test below, `random.randint` is replaced by a function that always returns `1`. The modification is in force only in that test.\n",
    "\n",
    "```python\n",
    "def test_single_step_one(mocker):\n",
    "    mocker.patch('random.randint', return_value=1)\n",
    "    b = Board(chutes=[], ladders=[])\n",
    "    pl = Player(b)\n",
    "    pl.move()\n",
    "    assert pl.position == 1\n",
    "```\n",
    "\n",
    "- `mocker` is automatically provided by py.test if the `pytest-mock` extension is installed, no imports required"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: Counting the number of calls to a method\n",
    "\n",
    "- See `examples/biolab_project/biolab/tests/test_dish.py`\n",
    "\n",
    "```python\n",
    "    def test_dish_aging_calls(self, mocker):\n",
    "        mocker.spy(Bacteria, 'ages')\n",
    "\n",
    "        n_a, n_b = 10, 20\n",
    "        d = Dish(n_a, n_b)\n",
    "        d.aging()\n",
    "\n",
    "        assert Bacteria.ages.call_count == n_a + n_b\n",
    "```\n",
    "\n",
    "- `mocker.spy()` wraps `Bacteria.ages` so we can extract information later\n",
    "- `Bacteria.ages.call_count` gives the number of times `Bacteria.ages` has been called\n",
    "- The \"spy\" has an effect only inside this test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": true
   },
   "source": [
    "## Tests involving randomness\n",
    "\n",
    "- Test methods that depend on random numbers\n",
    "- Exact results will depend on precise sequence of random numbers generated, i.e., on the random generator used and the random seed\n",
    "\n",
    "### Brute-force approaches\n",
    "\n",
    "#### Fixed seed\n",
    "By seeding the random number generator with a fixed value, we can ensure that we always get the same sequence of random numbers; particularly important while debugging.\n",
    "\n",
    "- Requires that we know which random number generator is used by methods tested\n",
    "- Adding more tests or changing tests or code can change the way in which random numbers are consumed\n",
    "\n",
    "#### Mocking\n",
    "Mock the random number function to return a fixed value.\n",
    "\n",
    "- Allows us to check that the code using the random numbers works as expected\n",
    "- Does not test whether the result has the expected distribution\n",
    "- Requires that we know exactly how the code draws random numbers (glass box testing (aka white box testing))\n",
    "\n",
    "### Statistical tests\n",
    "\n",
    "- The principal approach is based on statistical testing of hypothesis\n",
    "    - Formulate a hypothesis (expectation), e.g., \"value $x$ is a sample of random variable $X$ which has a normal (Gaussian) distribution of given mean $\\mu$ and variance $\\sigma$\"\n",
    "    - Find the $p$-value of $x$, i.e., the probability to observe a value at least as far from the mean as $x$ if $x$ indeed follows the assumed distribution\n",
    "    - Compare the $p$-value to a predefined acceptance limit $\\alpha$: if $p>\\alpha$ the test is passed\n",
    "- Interpretation: Let, e.g., $\\alpha=0.01=1\\%$. If we observe a value $x$ with a $p$-value less than $\\alpha=1\\%$, this means that the value $x$ belongs to the outer tail of the assumed distribution, among those values that make up the 1% least likely values in the distribution. We thus assume that $x$ did not come from the expected distribution and declare the test failed.\n",
    "- Note: By construction, this test will fail in 1% of all cases even if $x$ follows the assumed distribution. Thus, failures need to be inspected carefully.\n",
    "- See, e.g., Knuth, The Art of Computer Programming, vol 2.\n",
    "\n",
    "#### Examples of statistical tests\n",
    "\n",
    "- [$Z$-test](https://en.wikipedia.org/wiki/Z-test)\n",
    "    - Strictly speaking, tests whether the mean of $n$ random values drawn independently from the same distribution is from a Gaussian distribution of given mean and variance \n",
    "    - Due to the [central limit theorem](https://en.wikipedia.org/wiki/Central_limit_theorem), it can also be applied in many other cases as an approximation provided we are considering averages of many trials\n",
    "    - If the variance of the Gaussian distribution is not know a priori, one should use [Student's $t$-test](https://en.wikipedia.org/wiki/Student%27s_t-test) instead\n",
    "\n",
    "- [Binomial test](https://en.wikipedia.org/wiki/Binomial_test)\n",
    "    - An explicit test for [binomially distributed quantities](https://en.wikipedia.org/wiki/Binomial_distribution), e.g., the number of successes in $n$ Bernoulli experiments (coin flips)\n",
    "    - See also [GraphPad](https://www.graphpad.com/guides/prism/latest/statistics/stat_binomial.htm) for an explanation of the test. \n",
    "    - SciPy has an [older](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binom_test.html#scipy.stats.binom_test) and a [newer](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.binomtest.html#scipy.stats.binomtest) implementation of the binomial test available\n",
    "        - The older variant is *deprecated* and be removed in a future version of SciPy\n",
    "        - Use the newer version in new code\n",
    "        - The difference is in details of arguments needed and how the result is returned\n",
    "        - We will see in the BioLab example how to convert from old to new\n",
    "- `scipy.stats` provides [a number of statistical test functions](https://docs.scipy.org/doc/scipy/reference/stats.html#statistical-tests)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
